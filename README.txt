== Introduction ==
Integrates PBS Cove videos with Media module (2.x and higher). This enables
PBS videos to be added as Media Internet sources.

== History ==
This work is a cleaned-up version of https://github.com/ClusterFCK/media_pbs,
which itself is based on http://drupal.org/project/media_pbs and
https://drupal.org/project/media_youtube.

== Current project page ==
This project is currently a sandbox project at
https://drupal.org/sandbox/krisahil/2270311. I'd like to move it to be the
official 7.x release of media_pbs (https://drupal.org/project/media_pbs), so
I applied for co-maintainership of that project.

== Installation ==
First, install and configure Cove API module:
https://drupal.org/project/cove_api.
You need to enter your Cove API details so this media_pbs can fetch video
metadata.

Then, install as any other module.

You can configure both Cove API and media_pbs settings at
admin/config/media/cove_api.

== Usage ==
Navigate to file/add/web. You should see "PBS" in the list of supported
providers. Enter the URL for a PBS video
(e.g. http://video.pbs.org/video/2365248651/). If successful, Drupal will
create a new File entity, and the the URI will be something like
pbs://v/2365248651.

Also, the thumbnail image for that video should display when editing that
entity.

== TODO ==
media browser integration / styles 2.x / chapter bar support / iframe (partner player)
