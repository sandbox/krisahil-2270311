<?php

/**
 * @file
 * Create a PBS Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $pbs = new MediaPBSStreamWrapper('pbs://v/[video-code]');.
 */
class MediaPBSStreamWrapper extends MediaReadOnlyStreamWrapper {

  // Overrides $base_url defined in MediaReadOnlyStreamWrapper.
  protected $base_url = 'http://video.pbs.org/video';

  /**
   * Returns the PBS video mime type.
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/pbs';
  }

  /**
   * Returns a url in the format "http://pbs.tv/recorded/123456".
   *
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   * This is an exact copy of the function in MediaReadOnlyStreamWrapper,
   * here in case that example is redefined or removed.
   */
  public function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->base_url . '/' . $parameters['v'];
    }
  }

  /**
   * Returns URL of original image from Cove API.
   *
   * @return string
   *   URL of image for this video from Cove API.
   */
  public function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    return media_pbs_get_image_url($parts['v'], variable_get('media_pbs_thumbnail_size', 'Mezzanine'));
  }

  /**
   * Returns path to local video thumbnail.
   *
   * @return string
   *   Local path of image for this video from Cove API.
   */
  public function getLocalThumbnailPath() {
    $parts = $this->get_parameters();

    $local_path = 'public://media-pbs/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());
      if (!isset($response->error)) {
        file_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }
    return $local_path;
  }
}
