<?php

/**
 * @file
 * Contains MediaInternetPBSHandler.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetPBSHandler extends MediaInternetBaseHandler {

  /**
   * Check if a PBS video id is valid.
   *
   * @return bool
   *   Returns true if URL for this video ID returns 200/ok response.
   */
  static public function validId($id) {
    $url = 'http://video.pbs.org/video/' . $id . '/';
    $response = drupal_http_request($url, array('method' => 'HEAD'));
    if (200 != $response->code) {
      throw new MediaInternetValidationException('The PBS Cove video ID is invalid or the video was deleted.');
    }

    return TRUE;
  }

  /**
   * Parses the embed code to find the video's ID.
   *
   * @param string $embed_code
   *   Raw embed code or URL to be parsed.
   *
   * @return string
   *   The file stream wrapper URI to this Drupal file object.
   */
  public function parse($embed_code) {
    $patterns = array(
      // http://video.pbs.org/video/2282030210.
      '@\.pbs\.org/video/([^/\? ]+)@i',
      '@\.pbs\.org/video/media/swf/PBSPlayer\.swf" > </param><param name="flashvars" value="video=([^\&" ]+)@i',
      '@\.pbs\.org/widget/partnerplayer/([^"\?/ ]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embed_code, $matches);
      if (isset($matches[1]) && self::validId($matches[1])) {
        return file_stream_wrapper_uri_normalize('pbs://v/' . $matches[1]);
      }
    }
  }

  /**
   * Determines if this handler should claim the item.
   *
   * @param string $embed_code
   *   A string of user-submitted embed code.
   *
   * @return bool
   *   Pass TRUE to claim the item.
   */
  public function claim($embed_code) {
    if ($this->parse($embed_code)) {
      return TRUE;
    }
  }

  /**
   * Returns a file object which can be used for validation.
   *
   * @return StdClass
   *   File entity object.
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    // If this is a new file (no "fid"), then fetch the title from Cove API.
    if (empty($file->fid) && !preg_match('[^0-9]', $file->filename)) {
      $video = media_pbs_get_video($file->filename);
      if ($video && !empty($video->title)) {
        $file->filename = truncate_utf8($video->title, 255);
      }
    }

    return $file;
  }
}
