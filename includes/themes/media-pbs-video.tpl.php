<?php

/**
 * @file
 * Template file for theme('media_pbs_video').
 *
 * Variables available:
 *  $uri - The media uri for the PBS video (e.g., pbs://v/xsy7x8c9).
 *  $video_id - The unique identifier of the PBS video (e.g., xsy7x8c9).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Youtube iframe.
 *  $options - An array containing the Media Youtube formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Youtube file display options.
 *  $height - The height value set in Media: Youtube file display options.
 */

?>
<div class="media-pbs-outer-wrapper media-pbs-<?php print $id; ?>" id="media-pbs-<?php print $id; ?>"‘>
  <div class="media-pbs-preview-wrapper" id="<?php print $wrapper_id; ?>" >
    <?php print $output; ?>
  </div>
</div>
