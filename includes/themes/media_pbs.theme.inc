<?php

/**
 * @file
 * Theme and preprocess functions for Media: PBS.
 */

/**
 * Preprocess function for theme('media_pbs_video').
 */
function media_pbs_preprocess_media_pbs_video(&$variables) {

  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);

  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['v']);

  // Grab the video from PBS.
  $video = media_pbs_get_video($variables['video_id']);

  // Add some options as their own template variables.
  foreach (array('width', 'height') as $themevar) {
    $variables[$themevar] = $variables['options'][$themevar];
  }

  $variables['wrapper_id'] = 'media_pbs_' . $variables['video_id'] . '_' . $variables['id'];

  // Get the prepared iframe for displaying the video.
  $variables['output'] = $video->partner_player;

  // Pass the settings to replace the object tag with an iframe.
  $settings = array(
    'media_pbs' => array(
      $variables['wrapper_id'] => array(
        'width' => $variables['options']['width'],
        'height' => $variables['options']['height'],
        'video_id' => $variables['video_id'],
        'fullscreen' => $variables['options']['fullscreen'],
        'id' => $variables['wrapper_id'] . '_iframe',
      ),
    ),
  );
}
